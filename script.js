

function floor(input_szam){
	return Math.floor(input_szam)
}

function random(min, max, floored){
	var r = Math.random()
	var ossz = ( max - min ) * r + min
	return floored ? floor(ossz) : ossz
}

var listke = {
	szoveg: ["az idő nyeritett akkor azaz papagályosan kinyitotta a szárnyait", "az urak nem tudnak két lábbal lépni mint a verebek", "a folyók hajlandók darabokra törni ha sietni akarnak", "néhányszor üzletembernek lenni", "a élet zajok, színek, szellemi ritmusok egyidejű zűrzavarában jelentkezik, amelyet a dadaista művészet kiáltásokkal és heves lázzal azonnal, közvetlenül a maga hétköznapi lelkiségében és egész brutális valóságában ad vissza.", "kezük és szívük egyaránt vérzik", "lelkem kihűlt kemencéjében őrizlek, fehér vagy, akár egy vadászsólyom hallgatása.", "valóban hippiséget, huligánságot, frivolitást, linkséget, hülyék, csalók, álesztéták, álfilozófusok, bohócok nyílt összeesküvését látom", "a mozgalomnak középponti műfaját látom, a konstruktivista geometriák mellett, a montázsban", "az egész világnak és a művészetnek évezredeken keresztül a vallás volt a centruma. most mindenki vallástalan... egész vígan megvannak nélküle, ugye... hát éppúgy meglehetnek művészet nélkül is. tehát vagy válogatunk a régiből, ismétlünk, imitálunk, vagy jön a semmi, vagy pedig bízunk a jövőben, hogy jön valami kiszámíthatatlan.", "egyszer csak megértettem, hogy nem kell az életet túl sok súllyal, túl sok tennivalóval terhelni, olyasfélével, mint asszony, gyerekek, vidéki ház, autó.", "nyilván", "már jött is a félkrisztus faszobrász", "A szót akarom, ahol kezdődik és ahol vége van", " felmerülnek a szavak, a szavak válla; a szavak lába, karja, keze. ay, oi, u. nem szabad túl sok szót felengedni.", "ez az elátkozott nyelv, amelyhez ugyanúgy mocsok tapad, mint ahhoz a koszos kézhez, amely pénzt érintett.", "a szót akarom, ahol kezdődik és ahol vége van", "hol kúszva surranunk, a légi dróton, hol sok kötél sután egymást szeli", "ha egy ív hét könyök hosszú, akkor hét könyök hosszú szavakat akarok hozzáilleszteni", "lenn tapsol már a zöld isten, Koko. a legvénebbik bűnbak is begerjed.", "unokájára hull a mennyezet", "egy rövid lélegzetnyi idő alatt, a cselekvés ellen vagyok", "sem mellette, sem ellene nem vagyok, és senkinek nem akarok megmagyarázni semmit", "mintha robbanás történne bizonyos szavak jelentésében, többet érnek, mint amennyit a szótár szerint jelentenek.", "<img src='media/d0.jpg' title='Kassák Lajos'>", "<img src='media/d1.jpg' title='Andre Breton'>", "<img src='media/d2.jpg' title='Marcel Duchamp Fountain'>", "<img src='media/d3.jpg' title='Hugo Ball'>", "<img src='media/d4.jpg' title='Tristan Tzara'>", "<img src='media/d5.jpg' title='Raoul Hausmann, John Heartfield, and George Grosz'>", "<img src='media/d6.jpg' title='Kassák Lajos'>", "<img src='media/d7.jpg' title='Tristan Tzara'>", "<img src='media/d8.jpg' title='Moholy-Nagy László'>", "<img src='media/d9.jpg' title='Richard Huelsenbeck'>", "<img src='media/d10.jpg' title='Moholy-Nagy László'>", "<img src='media/d11.jpg' title='Hugo Ball'>","<img src='media/d12.jpg' title='Raoul Hausmann'>", "<img src='media/d13.jpg' title='Hugo Ball'>", "<img src='media/d14.jpg' title='Moholy-Nagy László'>", "<img src='media/d15.jpg' title='Kassák Lajos'>", "<img src='media/d16.jpg' title='Richard Huelsenbeck'>", "<img src='media/d17.jpg' title='Raoul Hausmann'>", "<img src='media/d18.jpg' title='Tristan Tzara'>", "<img src='media/d20.jpg' title='Victor Vasarely'>", "sohasem az esztétikai gyönyörködés diktálta", "az egyediség hiánya", "egy megjegyzés ehhez az ördögi körhöz", "a műsorrendből mindent elfelejt", "megismerés, megismerés, megismerés bum-bum, bum-bum, bum-bum", "a pokolba kell vetni a gondviselés kezét, az égre kell szegezni a pokol szemeit", "színek állandó ordítása", "minden groteszk jelenség, minden következetlenség találkozása", "A szó, a szó, a baj pont itt van, a szó, uraim, elsőrangú közügy."]
	,
	linki: ["nyilván", "keletkezik az artikulált nyelv", "hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget", "êletem végén még szerencsésebb vagyok, mint az elején", "ez a jelentő semmi, amin semmi sem jelent semmit", "minden ország céltalanjai, egyesüljetek", "semmivel akarjuk befejezni a háborút", "az a vidám ötletem támadt, hogy egy biciklikereket egy konyhaszékre szereljek, és nézzem, hogyan forog", "hogy vívja ki az ember az örök boldogságot?", "a kapcsolat a fontos, és az, hogy előbb egy kicsit megszakad. nem akarok olyan szavakat, amiket mások találtak ki", "a saját rendbontásomat akarom, és hozzá a megfelelő magánhangzókat és mássalhangzókat", "keletkezik az artikulált nyelv", "a művészet szenvedélyt kiváltó drog, a néző számára sokkal inkább", "ez a jelentő semmi, amin semmi sem jelent semmit", "New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe", "vasalódeszkának használni egy Rembrandtot", "a bejáratnál olajos színészné. combjába ütve arany-flitterek.",  "ívlámpa szúrná szemét, aki nézné", "lehet ellentmodásos cselekedeteket is egyszerre végrehajtani","hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget", "kis görbe, zöld csövecskék arra járnak", "az úristen megfeledkezik a szépasszonyokról", "fiatal volt és gyalázatosan igazságszagú", "inkább politikusnak, mint művésznek", "egész egyszerűen csak elejtem a hangokat.", "beérjük szavak és nyelv nélkül",  "remélem, egyszer eljutunk oda, hogy meg lehet élni anélkül is, hogy az ember rákényszerüljön arra, hogy dolgozzék", "de az állítás ellen is vagyok", "a kapcsolat a fontos, és az, hogy előbb egy kicsit megszakad. nem akarok olyan szavakat, amiket mások találtak ki", "hát mindenbe bele kell kotyognunk?", "mert gyűlölöm a jóízlést", "és a folytonos ellentmondást szeretem", "nahát", "a megállapítás és kijelentés ellen is vagyok", "annyi okos ember vitázott már annyi könyvben", "ti, nagyrabecsült költők, akik mindig szavakkal írtatok verset, de sohasem a szót írtátok meg", "az őrületig, az eszméletlenségig.", "hogy lesz az ember híres?", "olyan verseket olvasok, amelyeknek nincs más céljuk, mint hogy lemondjanak a nyelvről.", "minden szót más talált ki."], 
	kirak: function(){
		cimet()
		linket()
		var ri = (random(35, 200, true));
		$(".valami").css("font-size", ri)
		$("a").css("font-size", 30)
	},
	adj : function(mit){
		var x = random(0, listke[mit].length, true)
		return listke[mit][x]
	}
}

function cimet(){
	var cim = listke.adj("szoveg") + " " + "<br>"
	$(".valami").html(cim)

}	

function linket(){
	var hany = random(1, 4, true) 
	var araj = []
	
	for (var i = 0; i < hany ; i++) {
		var hh = $("<a>", {click: listke.kirak, html: listke.adj("linki")})
	    var most = (cs) => hh.html() === cs.html()


		if (!araj.find(most)) {
			araj.push(hh)
			-i	
		}
		// hh.html()
		
			// araj.push($("<a>", {click: listke.kirak, html: listke.adj("linki")}))


	}

	$(".linko").html(araj)
	

}


	var gomb = $("<button>", 
		{
		width: "50px", 
		height: "20px", 
		text: "forrás",
		color: "orange"
		}
	)
	//var forras = 1


$(document).ready(function(){
	$("body").append(listke.kirak())
	
	$("#tartalom").hide()
	$(gomb).click(function(){$(tartalom).slideToggle("slow").css({color: "grey"})
	$('#tzara').click(function(){
		window.open('tzara.html', '', 'width=600,height=600')
		return false
	})
	$('#hugo').click(function(){
		window.open("hugo.html", "", "width=600,height=600,top=200")
		return false
	})
	$('#kabar').click(function(){
		window.open("kabare.html", "", "width=600,height=600,top=700,left=300")
		return false
	})
	$('#magy').click(function(){
		window.open('magyar.html', '', 'width=600,height=600,top=500,left=900')
		return false
	})
	$('#nyolc').click(function(){
		window.open('nyolc.html', '', 'width=600,height=600,left=500')
		return false
	})
	$('#ready').click(function(){
		window.open('ready.html', '', 'width=600,height=600,left=900')
		return false
	})
})
})