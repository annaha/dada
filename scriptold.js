var listi = {
  "passages": [
    {
      "text": "Az idő nyeritett akkor azaz papagályosan kinyitotta a\nszárnyait \n\n\n[[nyilván]]\n[[keletkezik az artikulált nyelv]]\n[[Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget]]\n[[Életem végén még szerencsésebb vagyok, mint az elején]]",
      "links": [
        {
          "name": "nyilván",
          "link": "nyilván",
          "pid": "12"
        },
        {
          "name": "keletkezik az artikulált nyelv",
          "link": "keletkezik az artikulált nyelv",
          "pid": "4"
        },
        {
          "name": "Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget",
          "link": "Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget",
          "pid": "13"
        },
        {
          "name": "Életem végén még szerencsésebb vagyok, mint az elején",
          "link": "Életem végén még szerencsésebb vagyok, mint az elején",
          "pid": "27"
        }
      ],
      "name": "start",
      "pid": "1",
      "position": {
        "x": "575",
        "y": "25"
      }
    },
    
    {
      "text": "az urak nem tudnak két lábbal lépni mint a verebek\n\n[[Ez a jelentő semmi, amin semmi sem jelent semmit]]",
      "links": [
        {
          "name": "Ez a jelentő semmi, amin semmi sem jelent semmit",
          "link": "Ez a jelentő semmi, amin semmi sem jelent semmit",
          "pid": "5"
        }
      ],
      "name": "minden ország céltalanjai, egyesüljetek",
      "pid": "2",
      "position": {
        "x": "926.6666666666667",
        "y": "353.3333333333333"
      }
    },
    {
      "text": "a folyók hajlandók darabokra törni ha sietni akarnak\n\n[[semmivel akarjuk befejezni a háborút]]\n[[az a vidám ötletem támadt, hogy egy biciklikereket egy konyhaszékre szereljek, és nézzem, hogyan forog]]",
      "links": [
        {
          "name": "semmivel akarjuk befejezni a háborút",
          "link": "semmivel akarjuk befejezni a háborút",
          "pid": "6"
        },
        {
          "name": "az a vidám ötletem támadt, hogy egy biciklikereket egy konyhaszékre szereljek, és nézzem, hogyan forog",
          "link": "az a vidám ötletem támadt, hogy egy biciklikereket egy konyhaszékre szereljek, és nézzem, hogyan forog",
          "pid": "7"
        }
      ],
      "name": "Hogy vívja ki az ember az örök boldogságot?",
      "pid": "3",
      "position": {
        "x": "473.66666666666663",
        "y": "1017.3333333333334"
      }
    },
    {
      "text": "néhányszor üzletembernek lenni\n\n[[A kapcsolat a fontos, és az, hogy előbb egy kicsit megszakad. Nem akarok olyan szavakat, amiket mások találtak ki]]\n[[A saját rendbontásomat akarom, és hozzá a megfelelő magánhangzókat és mássalhangzókat]]",
      "links": [
        {
          "name": "A kapcsolat a fontos, és az, hogy előbb egy kicsit megszakad. Nem akarok olyan szavakat, amiket mások találtak ki",
          "link": "A kapcsolat a fontos, és az, hogy előbb egy kicsit megszakad. Nem akarok olyan szavakat, amiket mások találtak ki",
          "pid": "17"
        },
        {
          "name": "A saját rendbontásomat akarom, és hozzá a megfelelő magánhangzókat és mássalhangzókat",
          "link": "A saját rendbontásomat akarom, és hozzá a megfelelő magánhangzókat és mássalhangzókat",
          "pid": "18"
        }
      ],
      "name": "keletkezik az artikulált nyelv",
      "pid": "4",
      "position": {
        "x": "840.6666666666666",
        "y": "175"
      }
    },
    {
      "text": "A élet zajok, színek, szellemi ritmusok egyidejű zűrzavarában jelentkezik, amelyet a dadaista művészet kiáltásokkal és heves lázzal azonnal, közvetlenül a maga hétköznapi lelkiségében és egész brutális valóságában ad vissza.\n\n[[a múvészet szevedélyt kiváltó drog, a néző számára sokkal inkább]]",
      "links": [
        {
          "name": "a múvészet szevedélyt kiváltó drog, a néző számára sokkal inkább",
          "link": "a múvészet szevedélyt kiváltó drog, a néző számára sokkal inkább",
          "pid": "8"
        }
      ],
      "name": "Ez a jelentő semmi, amin semmi sem jelent semmit",
      "pid": "5",
      "position": {
        "x": "656.6666666666666",
        "y": "595"
      }
    },
    {
      "text": "kezük és szívük egyaránt vérzik\n[[Életem végén még szerencsésebb vagyok, mint az elején]]",
      "links": [
        {
          "name": "Életem végén még szerencsésebb vagyok, mint az elején",
          "link": "Életem végén még szerencsésebb vagyok, mint az elején",
          "pid": "27"
        }
      ],
      "name": "semmivel akarjuk befejezni a háborút",
      "pid": "6",
      "position": {
        "x": "490",
        "y": "305"
      }
    },
    {
      "text": "Lelkem kihűlt kemencéjében őrizlek,\nFehér vagy, akár egy vadászsólyom hallgatása.\n\n[[New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe]]\n[[vasalódeszkának használni egy Rembrandtot]]\n[[A bejáratnál olajos színészné. Combjába ütve arany-flitterek.]]",
      "links": [
        {
          "name": "New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe",
          "link": "New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe",
          "pid": "9"
        },
        {
          "name": "vasalódeszkának használni egy Rembrandtot",
          "link": "vasalódeszkának használni egy Rembrandtot",
          "pid": "10"
        },
        {
          "name": "A bejáratnál olajos színészné. Combjába ütve arany-flitterek.",
          "link": "A bejáratnál olajos színészné. Combjába ütve arany-flitterek.",
          "pid": "11"
        }
      ],
      "name": "az a vidám ötletem támadt, hogy egy biciklikereket egy konyhaszékre szereljek, és nézzem, hogyan forog",
      "pid": "7",
      "position": {
        "x": "180",
        "y": "545"
      }
    },
    {
      "text": "valóban hippiséget, huligánságot, frivolitást, linkséget, hülyék, csalók, álesztéták, álfilozófusok, bohócok nyílt összeesküvését látom\n\n[[Ívlámpa szúrná szemét, aki nézné]]",
      "links": [
        {
          "name": "Ívlámpa szúrná szemét, aki nézné",
          "link": "Ívlámpa szúrná szemét, aki nézné",
          "pid": "20"
        }
      ],
      "name": "a múvészet szevedélyt kiváltó drog, a néző számára sokkal inkább",
      "pid": "8",
      "position": {
        "x": "1053.3333333333335",
        "y": "845"
      }
    },
    {
      "text": "a mozgalomnak középponti műfaját látom, a konstruktivista geometriák mellett, a montázsban\n[[lehet ellentmodásos cselekedeteket is egyszerre végrehajtani]]",
      "links": [
        {
          "name": "lehet ellentmodásos cselekedeteket is egyszerre végrehajtani",
          "link": "lehet ellentmodásos cselekedeteket is egyszerre végrehajtani",
          "pid": "21"
        }
      ],
      "name": "New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe",
      "pid": "9",
      "position": {
        "x": "190",
        "y": "871.6666666666667"
      }
    },
    {
      "text": "Az egész világnak és a művészetnek évezredeken keresztül a vallás volt a centruma. Most mindenki vallástalan... egész vígan megvannak nélküle, ugye... hát éppúgy meglehetnek művészet nélkül is. Tehát vagy válogatunk a régiből, ismétlünk, imitálunk, vagy jön a semmi, vagy pedig bízunk a jövőben, hogy jön valami kiszámíthatatlan.\n\n[[Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget]]",
      "links": [
        {
          "name": "Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget",
          "link": "Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget",
          "pid": "13"
        }
      ],
      "name": "vasalódeszkának használni egy Rembrandtot",
      "pid": "10",
      "position": {
        "x": "91.66666666666666",
        "y": "310"
      }
    },
    {
      "text": "Egyszer csak megértettem, hogy nem kell az életet túl sok súllyal, túl sok tennivalóval terhelni, olyasfélével, mint asszony, gyerekek, vidéki ház, autó. \n[[Kis görbe, zöld csövecskék arra járnak]]",
      "links": [
        {
          "name": "Kis görbe, zöld csövecskék arra járnak",
          "link": "Kis görbe, zöld csövecskék arra járnak",
          "pid": "26"
        }
      ],
      "name": "A bejáratnál olajos színészné. Combjába ütve arany-flitterek.",
      "pid": "11",
      "position": {
        "x": "525",
        "y": "630"
      }
    },
    {
      "text": "nyilván\naz uristen megfeledkezik a szépasszonyokról\nmár jött is a félkrisztus faszobrász\nfiatal volt és gyalázatosan igazságszagu\ninkább politikusnak, mint művésznek \n\n[[Egész egyszerűen csak elejtem a hangokat.]]\n[[beérjük szavak és nyelv nélkül]]",
      "links": [
        {
          "name": "Egész egyszerűen csak elejtem a hangokat.",
          "link": "Egész egyszerűen csak elejtem a hangokat.",
          "pid": "14"
        },
        {
          "name": "beérjük szavak és nyelv nélkül",
          "link": "beérjük szavak és nyelv nélkül",
          "pid": "15"
        }
      ],
      "name": "nyilván",
      "pid": "12",
      "position": {
        "x": "577",
        "y": "163"
      }
    },
    {
      "text": "A szót akarom, ahol kezdődik és ahol vége van",
      "name": "Hogy lehet levetkezni minden sikamlósságot és zsurnalizmust, minden kedvességet és fességet, minden túlmoralizáltságot, elállatiasodottságot és mesterkéltséget",
      "pid": "13",
      "position": {
        "x": "417.3333333333333",
        "y": "113.66666666666667"
      }
    },
    {
      "text": " Felmerülnek a szavak, a szavak válla; a szavak lába, karja, keze. Ay, oi, u. Nem szabad túl sok szót felengedni.\n \n [[New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe]]",
      "links": [
        {
          "name": "New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe",
          "link": "New Yorkban egy vasboltban vettem egy hólapátot, és ráírtam: egy törött kar elébe",
          "pid": "9"
        }
      ],
      "name": "Egész egyszerűen csak elejtem a hangokat.",
      "pid": "14",
      "position": {
        "x": "1310.3333333333335",
        "y": "874"
      }
    },
    {
      "text": "Ez az elátkozott nyelv, amelyhez ugyanúgy mocsok tapad, mint ahhoz a koszos kézhez, amely pénzt érintett.\n\n[[A szót akarom, ahol kezdődik és ahol]]\n[[Remélem, egyszer eljutunk oda, hogy meg lehet élni anélkül is, hogy az ember rákényszerüljön arra, hogy dolgozzék]]",
      "links": [
        {
          "name": "A szót akarom, ahol kezdődik és ahol",
          "link": "A szót akarom, ahol kezdődik és ahol",
          "pid": "16"
        },
        {
          "name": "Remélem, egyszer eljutunk oda, hogy meg lehet élni anélkül is, hogy az ember rákényszerüljön arra, hogy dolgozzék",
          "link": "Remélem, egyszer eljutunk oda, hogy meg lehet élni anélkül is, hogy az ember rákényszerüljön arra, hogy dolgozzék",
          "pid": "25"
        }
      ],
      "name": "beérjük szavak és nyelv nélkül",
      "pid": "15",
      "position": {
        "x": "1375",
        "y": "69.33333333333331"
      }
    },
    {
      "text": " vége van\n\n[[az a vidám ötletem támadt hogy egy biciklikereket egy konyhaszékre szereljek és nézzem hogyan forog]]\n[[vasalódeszkának használni egy Rembrandtot]]",
      "links": [
        {
          "name": "az a vidám ötletem támadt hogy egy biciklikereket egy konyhaszékre szereljek és nézzem hogyan forog",
          "link": "az a vidám ötletem támadt hogy egy biciklikereket egy konyhaszékre szereljek és nézzem hogyan forog",
          "pid": "31"
        },
        {
          "name": "vasalódeszkának használni egy Rembrandtot",
          "link": "vasalódeszkának használni egy Rembrandtot",
          "pid": "10"
        }
      ],
      "name": "A szót akarom, ahol kezdődik és ahol",
      "pid": "16",
      "position": {
        "x": "872.6666666666666",
        "y": "487.66666666666663"
      }
    },
    {
      "text": "Hol kúszva surranunk, a légi dróton,\nHol sok kötél sután egymást szeli\n\n[[de az állítás ellen is vagyok]]",
      "links": [
        {
          "name": "de az állítás ellen is vagyok",
          "link": "de az állítás ellen is vagyok",
          "pid": "23"
        }
      ],
      "name": "A kapcsolat a fontos, és az, hogy előbb egy kicsit megszakad. Nem akarok olyan szavakat, amiket mások találtak ki",
      "pid": "17",
      "position": {
        "x": "1012",
        "y": "1118"
      }
    },
    {
      "text": "Ha egy ív hét könyök hosszú, akkor hét könyök hosszú szavakat akarok hozzáilleszteni\n\n[[Hát mindenbe bele kell kotyognunk? ]]\n[[Ívlámpa szúrná szemét, aki nézné]]\n",
      "links": [
        {
          "name": "Hát mindenbe bele kell kotyognunk? ",
          "link": "Hát mindenbe bele kell kotyognunk? ",
          "pid": "19"
        },
        {
          "name": "Ívlámpa szúrná szemét, aki nézné",
          "link": "Ívlámpa szúrná szemét, aki nézné",
          "pid": "20"
        }
      ],
      "name": "A saját rendbontásomat akarom, és hozzá a megfelelő magánhangzókat és mássalhangzókat",
      "pid": "18",
      "position": {
        "x": "764.3333333333333",
        "y": "24"
      }
    },
    {
      "text": "Lenn tapsol már a zöld isten, Koko.\nA legvénebbik bűnbak is begerjed.\n\n[[lehet ellentmodásos cselekedeteket is egyszerre végrehajtani]]\n[[de az állítás ellen is vagyok]]",
      "links": [
        {
          "name": "lehet ellentmodásos cselekedeteket is egyszerre végrehajtani",
          "link": "lehet ellentmodásos cselekedeteket is egyszerre végrehajtani",
          "pid": "21"
        },
        {
          "name": "de az állítás ellen is vagyok",
          "link": "de az állítás ellen is vagyok",
          "pid": "23"
        }
      ],
      "name": "Hát mindenbe bele kell kotyognunk? ",
      "pid": "19",
      "position": {
        "x": "995.3333333333334",
        "y": "220.66666666666669"
      }
    },
    {
      "text": "Unokájára hull a mennyezet\n\n[[mert gyűlölöm a jóízlést]]",
      "links": [
        {
          "name": "mert gyűlölöm a jóízlést",
          "link": "mert gyűlölöm a jóízlést",
          "pid": "24"
        }
      ],
      "name": "Ívlámpa szúrná szemét, aki nézné",
      "pid": "20",
      "position": {
        "x": "1001.6666666666666",
        "y": "83.33333333333334"
      }
    },
    {
      "text": "egy rövid lélegzetnyi idő alatt, a cselekvés ellen vagyok\n[[és a folytonos ellentmondást szeretem]]",
      "links": [
        {
          "name": "és a folytonos ellentmondást szeretem",
          "link": "és a folytonos ellentmondást szeretem",
          "pid": "22"
        }
      ],
      "name": "lehet ellentmodásos cselekedeteket is egyszerre végrehajtani",
      "pid": "21",
      "position": {
        "x": "1557",
        "y": "742.3333333333334"
      }
    },
    {
      "text": "[[semmivel akarjuk befejezni a háborút]]",
      "links": [
        {
          "name": "semmivel akarjuk befejezni a háborút",
          "link": "semmivel akarjuk befejezni a háborút",
          "pid": "6"
        }
      ],
      "name": "és a folytonos ellentmondást szeretem",
      "pid": "22",
      "position": {
        "x": "1192",
        "y": "409"
      }
    },
    {
      "text": "sem mellette, sem ellene nem vagyok, és senkinek nem akarok megmagyarázni semmit\n\n[[nahát]]",
      "links": [
        {
          "name": "nahát",
          "link": "nahát",
          "pid": "29"
        }
      ],
      "name": "de az állítás ellen is vagyok",
      "pid": "23",
      "position": {
        "x": "1693.3333333333335",
        "y": "225.66666666666666"
      }
    },
    {
      "text": "Mintha robbanás történne bizonyos szavak jelentésében, többet érnek, mint amennyit a szótár szerint jelentenek.\n[[piros]]",
      "links": [
        {
          "name": "piros",
          "link": "piros",
          "pid": "28"
        }
      ],
      "name": "mert gyűlölöm a jóízlést",
      "pid": "24",
      "position": {
        "x": "1707.3333333333335",
        "y": "54.66666666666667"
      }
    },
    {
      "text": "[[start]]\n[[Hát mindenbe bele kell kotyognunk? ]]",
      "links": [
        {
          "name": "start",
          "link": "start",
          "pid": "1"
        },
        {
          "name": "Hát mindenbe bele kell kotyognunk? ",
          "link": "Hát mindenbe bele kell kotyognunk? ",
          "pid": "19"
        }
      ],
      "name": "Remélem, egyszer eljutunk oda, hogy meg lehet élni anélkül is, hogy az ember rákényszerüljön arra, hogy dolgozzék",
      "pid": "25",
      "position": {
        "x": "1042",
        "y": "640.3333333333334"
      }
    },
    {
      "text": "[[nyilván]]",
      "links": [
        {
          "name": "nyilván",
          "link": "nyilván",
          "pid": "12"
        }
      ],
      "name": "Kis görbe, zöld csövecskék arra járnak",
      "pid": "26",
      "position": {
        "x": "793.3333333333334",
        "y": "940"
      }
    },
    {
      "text": "[[minden ország céltalanjai, egyesüljetek]]\n[[Hogy vívja ki az ember az örök boldogságot?]]",
      "links": [
        {
          "name": "minden ország céltalanjai, egyesüljetek",
          "link": "minden ország céltalanjai, egyesüljetek",
          "pid": "2"
        },
        {
          "name": "Hogy vívja ki az ember az örök boldogságot?",
          "link": "Hogy vívja ki az ember az örök boldogságot?",
          "pid": "3"
        }
      ],
      "name": "Életem végén még szerencsésebb vagyok, mint az elején",
      "pid": "27",
      "position": {
        "x": "215.33333333333331",
        "y": "165"
      }
    },
    {
      "text": "<img class='kep' src='https://d32dm0rphc51dk.cloudfront.net/88JcXbBVkR2E24Vn3Xsyeg/larger.jpg'>\n\n[[ <img class='kisebb' src='https://images-na.ssl-images-amazon.com/images/I/21GI6DWI2kL._SL500_AC_SS350_.jpg'> ]]\n[[nahát]]",
      "links": [
        {
          "name": " <img class='kisebb' src='https://images-na.ssl-images-amazon.com/images/I/21GI6DWI2kL._SL500_AC_SS350_.jpg'> ",
          "link": " <img class='kisebb' src='https://images-na.ssl-images-amazon.com/images/I/21GI6DWI2kL._SL500_AC_SS350_.jpg'> "
        },
        {
          "name": "nahát",
          "link": "nahát",
          "pid": "29"
        }
      ],
      "name": "piros",
      "pid": "28",
      "position": {
        "x": "1224",
        "y": "133"
      }
    },
    {
      "text": "<div class='zold'>akarmi </div>\n[[Hát mindenbe bele kell kotyognunk? ]]",
      "links": [
        {
          "name": "Hát mindenbe bele kell kotyognunk? ",
          "link": "Hát mindenbe bele kell kotyognunk? ",
          "pid": "19"
        }
      ],
      "name": "nahát",
      "pid": "29",
      "position": {
        "x": "1495",
        "y": "232"
      }
    },
    {
      "text": "Double-click this passage to edit it.",
      "name": "  ",
      "pid": "30",
      "position": {
        "x": "1394.3333333333333",
        "y": "339.6666666666667"
      }
    },
    {
      "text": "Double-click this passage to edit it.",
      "name": "az a vidám ötletem támadt hogy egy biciklikereket egy konyhaszékre szereljek és nézzem hogyan forog",
      "pid": "31",
      "position": {
        "x": "856.6666666666666",
        "y": "697.6666666666666"
      }
    }
  ],
  "name": "dada",
  "startnode": "1",
  "creator": "Twine",
  "creator-version": "2.2.1",
  "ifid": "B3619E6D-4998-4508-9147-A146C70EA076"
}